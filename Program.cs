﻿// this try statement is simply to catch any possible errors that I had not considered
// it's probably not needed, but whatever.
try
{
    var ipAddress = System.Net.IPAddress.Parse("127.0.0.1");
    int port = 3000;
    int maxConnections = 10;

    // Retrieve the current directory and append the relative content path
    string contentPath = Path.Combine(Directory.GetCurrentDirectory(), "content");

    var server = new Server(contentPath);
    server.Start(ipAddress, port, maxConnections);

    Console.WriteLine($"Server started at http://{ipAddress}:{port}/");
    Console.WriteLine("Press any key to stop...");
    Console.ReadKey();

    server.Stop();
}
catch (Exception ex)
{
    Console.WriteLine($"Fatal error: {ex.Message}");
}
