using System.Text;

public class HttpResponse
{
    public string StatusCode { get; }
    public string ContentType { get; }
    public byte[] Content { get; }

    public HttpResponse(string statusCode, string contentType, string content)
    {
        StatusCode = statusCode;
        ContentType = contentType;
        Content = Encoding.UTF8.GetBytes(content);
    }

    public string GetHeader()
    {
        return $"HTTP/1.1 {StatusCode}\r\n" +
               $"Server: MySimpleWebServer\r\n" +
               $"Content-Length: {Content.Length}\r\n" +
               $"Content-Type: {ContentType}\r\n\r\n";
    }
}

