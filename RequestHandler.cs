using System;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;

public class RequestHandler
{
    private readonly Socket _clientSocket;
    private readonly string _contentPath;
    private readonly Encoding _charEncoder = Encoding.UTF8;
    private readonly ContentTypeProvider _contentTypeProvider;
    private readonly FileProvider _fileProvider;

    public RequestHandler(Socket clientSocket, string contentPath)
    {
        _clientSocket = clientSocket;
        _contentPath = contentPath;
        _contentTypeProvider = new ContentTypeProvider();
        _fileProvider = new FileProvider();
    }

    public void HandleRequest()
    {
        try
        {
            byte[] buffer = new byte[10240]; // 10kb
            int receivedByteCount = _clientSocket.Receive(buffer);
            var requestString = _charEncoder.GetString(buffer, 0, receivedByteCount);

            var (httpMethod, requestedUrl) = ParseRequest(requestString);

            Console.WriteLine($"Received request:\n{requestString}");

            HandleHttpRequest(httpMethod, requestedUrl);
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Request processing error: {ex.Message}");
        }
        finally
        {
            _clientSocket.Close();
        }
    }

    private (string HttpMethod, string RequestedUrl) ParseRequest(string request)
    {
        var requestLines = request.Split(new[] { Environment.NewLine }, StringSplitOptions.None);
        var requestLine = requestLines.First().Split(' ');

        if (requestLine.Length < 2)
            throw new InvalidOperationException("Invalid HTTP request line.");

        var httpMethod = requestLine[0];
        var requestedUrl = requestLine[1];

        return (httpMethod, requestedUrl);
    }

    private void HandleHttpRequest(string httpMethod, string requestedUrl)
    {
        string requestedFile = requestedUrl.Split('?').FirstOrDefault();
        requestedFile = requestedFile.Trim('/').Replace("/", Path.DirectorySeparatorChar.ToString());

        if (!httpMethod.Equals("GET", StringComparison.OrdinalIgnoreCase))
        {
            SendNotImplementedResponse();
            return;
        }

        if (!_fileProvider.FileExists(_contentPath, requestedFile))
        {
            SendNotFoundResponse();
            return;
        }

        var fileContent = _fileProvider.GetFileContent(_contentPath, requestedFile);
        var fileExtension = Path.GetExtension(requestedFile).TrimStart('.');
        var contentType = _contentTypeProvider.GetContentType(fileExtension);

        var response = new HttpResponse("200 OK", contentType, _charEncoder.GetString(fileContent));


        SendResponse(response);
    }


    private void SendNotFoundResponse()
    {
        var response = new HttpResponse("404 Not Found", "text/html", "<html><body>404 - Not Found</body></html>");
        SendResponse(response);
    }

    private void SendNotImplementedResponse()
    {
        var response = new HttpResponse("501 Not Implemented", "text/html", "<html><body>501 - Not Implemented</body></html>");
        SendResponse(response);
    }

    private void SendResponse(HttpResponse response)
    {
        var header = _charEncoder.GetBytes(response.GetHeader());
        _clientSocket.Send(header);
        _clientSocket.Send(response.Content);
    }
}
