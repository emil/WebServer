public class ContentTypeProvider
{
    private readonly Dictionary<string, string> _mimeTypes = new Dictionary<string, string>
    {
        {"htm", "text/html"},
        {"html", "text/html"},
        {"xml", "text/xml"},
        {"txt", "text/plain"},
        {"css", "text/css"},
        {"png", "image/png"},
        {"gif", "image/gif"},
        {"jpg", "image/jpeg"},
        {"jpeg", "image/jpeg"},
        {"zip", "application/zip"},
    };

    public string GetContentType(string fileExtension)
    {
        if (_mimeTypes.ContainsKey(fileExtension))
        {
            return _mimeTypes[fileExtension];
        }

        return "application/octet-stream"; // generic byte data
    }

}
