using System.Net;
using System.Net.Sockets;

public class Server
{
    public bool IsRunning { get; private set; }
    private Socket _serverSocket;
    private readonly string _contentPath;

    public Server(string contentPath)
    {
        _contentPath = contentPath;
    }

    public bool Start(IPAddress ipAddress, int port, int maxConnections)
    {
        if (IsRunning) return false;

        try
        {
            _serverSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            _serverSocket.Bind(new IPEndPoint(ipAddress, port));
            _serverSocket.Listen(maxConnections);

            Thread requestListenerThread = new Thread(ListenForRequests);
            requestListenerThread.Start();

            IsRunning = true;
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Server start error: {ex.Message}");
            return false;
        }

        return true;
    }

    private void ListenForRequests()
    {
        while (IsRunning)
        {
            try
            {
                var clientSocket = _serverSocket.Accept();
                var handler = new RequestHandler(clientSocket, _contentPath);
                var clientThread = new Thread(handler.HandleRequest);
                clientThread.Start();
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Request handling error: {ex.Message}");
            }
        }
    }

    public void Stop()
    {
        if (IsRunning)
        {
            try
            {
                _serverSocket.Close();
                IsRunning = false;
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Server stop error: {ex.Message}");
            }
        }
    }
}
