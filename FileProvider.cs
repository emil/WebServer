public class FileProvider
{
    public byte[] GetFileContent(string contentPath, string requestedFile)
    {
        string fullPath = null;
        try
        {
            fullPath = Path.Combine(contentPath, requestedFile.TrimStart('/').Replace('/', Path.DirectorySeparatorChar));
            return File.ReadAllBytes(fullPath);
        }
        catch (Exception ex)
        {
            if (fullPath != null)
            {
                Console.WriteLine($"File not found at: {fullPath}");
            }
            else
            {
                Console.WriteLine("Failed to generate fullPath.");
            }
            throw new FileNotFoundException("File not found.", ex);
        }
    }


    public bool FileExists(string contentPath, string requestedFile)
    {
        string fullPath = Path.Combine(contentPath, requestedFile.TrimStart('/').Replace('/', Path.DirectorySeparatorChar));
        return File.Exists(fullPath);
    }
}
